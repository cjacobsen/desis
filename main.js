$(document).ready(function () {
    // Helpers
    function fill_combo(id, data) {
        let element = $(`#${id}`);
        element
            .empty()
            .append($('<option>', { value: -1, text: '-' }));
        data.forEach(item => {
            element.append($('<option>', {
                value: item.id,
                text: item.text
            }));
        });
    }
    function clean_combo(id) {
        fill_combo(id, []);
    }

    // Init
    clean_combo('region');
    clean_combo('comuna');
    clean_combo('candidato');
    $.ajax({
        method: "POST",
        url: "get_regiones.php"
    })
        .done(function (regiones) {
            fill_combo('region', regiones);
            $('#region').on('change', function (element) {
                //console.log('Region: ' + $(this).val());
                clean_combo('comuna');
                clean_combo('candidato');
                $.ajax({
                    method: "POST",
                    url: "get_comunas.php",
                    data: { 'region': $(this).val() }
                })
                    .done(function (comunas) {
                        fill_combo('comuna', comunas);
                        $('#comuna')
                            .unbind('change')
                            .on('change', function (element) {
                                //console.log('Comuna: ' + $(this).val());
                                clean_combo('candidato');
                                $.ajax({
                                    method: "POST",
                                    url: "get_candidatos.php",
                                    data: { 'comuna': $(this).val() }
                                })
                                    .done(function (candidatos) {
                                        fill_combo('candidato', candidatos);
                                    });
                            });
                    });

            });
        });




    // Submit
    $("#votar").on("click", function () {
        $('[id^=msg_]').empty();
        $.ajax({
            method: "POST",
            url: "submit_form.php",
            data: {
                nombre: $('#nombre').val(),
                alias: $('#alias').val(),
                rut: $('#rut').val(),
                email: $('#email').val(),
                candidato: $('#candidato').val(),
                como: {
                    'web': $('#web').prop('checked'),
                    'tv': $('#tv').prop('checked'),
                    'rrss': $('#rrss').prop('checked'),
                    'amigo': $('#amigo').prop('checked')
                }
            }
        })
            .done(function (errores) {
                //console.log(errores);
                if (errores.length > 0) {
                    errores.forEach(element => {
                        $(`#${element.id}`).html(element.msg);
                    });
                    $('#msg_global')
                        .addClass('error');
                        //.html('Error');
                } else {
                    $('#msg_global')
                        .removeClass('error')
                        .html('Voto registrado');
                }
            });

    });

});