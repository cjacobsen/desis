# Prueba de Diagnóstico - Sistema de Votación

## Instalación

- Clonar repo en su directory root:
  ```bash
  git clone https://gitlab.com/cjacobsen/desis.git
  cd desis
  ```
- Importar base de datos MySQL:
  ```bash
  mysql desis < sql/db.sql
  ```
- Configurar credenciales de la DB en archivo `config.php`:
  ```php
  <?php
  $DB_NAME = 'desis';
  $DB_USER = 'desis_user';
  $DB_PASS = 'desis_pass';
  $DB_HOST = 'localhost';
  $DB_PORT = 3306;
  ```
- El formulario esta listo para recibir votaciones.

## Demo
URL: https://jacobsen.cl/desis/