<?php
$DB_NAME = 'desis';
$DB_USER = 'desis_user';
$DB_PASS = 'desis_pass';
$DB_HOST = 'localhost';
$DB_PORT = 3306;

$pdo = new PDO("mysql:host=$DB_HOST;port=$DB_PORT;dbname=$DB_NAME", $DB_USER, $DB_PASS);