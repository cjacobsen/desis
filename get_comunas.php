<?php
require('config.php');
require('helpers.php');
header('Content-Type: application/json; charset=utf-8');

//error_log(var_export($_POST, true));
$region = (int)$_POST['region'];
if($region == -1) {
    $comunas = [];
} else {
    $query = $pdo->prepare("SELECT id, nombre as text FROM comuna where region_id = ? order by nombre");
    $query->execute([$region]);
    $comunas = $query->fetchAll();
    //error_log(var_export($regiones, true));
}


echo json_encode($comunas);