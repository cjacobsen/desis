<?php
require('config.php');
require('helpers.php');
header('Content-Type: application/json; charset=utf-8');

//error_log(var_export($_POST, true));
$comuna = (int)$_POST['comuna'];
if($comuna == -1) {
    $candidatos = [];
} else {
    $query = $pdo->prepare("SELECT id, nombre as text FROM candidato where comuna_id = ? order by nombre");
    $query->execute([$comuna]);
    $candidatos = $query->fetchAll();
}


echo json_encode($candidatos);