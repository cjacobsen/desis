<?php
require('config.php');
require('helpers.php');
header('Content-Type: application/json; charset=utf-8');

$query = $pdo->query("SELECT id, nombre as text FROM region");
$regiones = $query->fetchAll();
//error_log(var_export($regiones, true));
echo json_encode($regiones);