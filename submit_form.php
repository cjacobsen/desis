<?php
require('config.php');
require('helpers.php');

header('Content-Type: application/json; charset=utf-8');
error_log(var_export($_POST, true));

$errores = [];

// Formateo
$nombre = trim($_POST['nombre']);
$alias = trim($_POST['alias']);
$rut = trim(str_replace(['.', '-'], ['', ''], $_POST['rut']));
$email = trim($_POST['email']);
$candidato = (int)$_POST['candidato'];
$como = (array)$_POST['como'];

// Validaciones
//   Nombre
if (!$nombre) {
    push_error('msg_nombre', 'Debe ingresar su nombre y apellido');
}
//   Alias
if (strlen($alias) <= 5) {
    push_error('msg_alias', 'El alias debe contener más de 5 caracteres');
}
if (!preg_match('/[a-zA-Z]/', $alias)) {
    push_error('msg_alias', 'El alias debe contener letras');
}
if (!preg_match('/[0-9]/', $alias)) {
    push_error('msg_alias', 'El alias debe contener números');
}
//   RUT
if (!rutValido($rut)) {
    push_error('msg_rut', 'El RUT no es válido');
}
// Email
if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    push_error('msg_email', 'El Email no es válido');
}
// Candidato
if($candidato < 0) {
    push_error('msg_candidato', 'Debe seleccionar un candidato');
}
// Como se enteró (Minimo 2)
$count = 0;
$valid_keys = ['web', 'tv', 'rrss', 'amigo'];
$set = [];
foreach($como as $key => $val) {
    if(!in_array($key, $valid_keys)) {
        push_error('msg_como', 'Item inválido');
        break;
    }
    if($val == 'true') {
        $set[] = $key;
        $count++;
    }
}
if($count < 2) {
    push_error('msg_como', 'Debe seleccionar al menos 2 opciones');
}


// Registrar voto
if (!$errores) {
    // Valida voto duplicado
    $query = $pdo->prepare("SELECT * FROM voto WHERE rut = ?");
    $response = $query->execute([$rut]);
    if($query->rowCount() > 0) {
        push_error('msg_global', 'Este RUT ya registra un voto.');
    } else {
        try {
            $query = $pdo->prepare("INSERT INTO voto SET candidato_id = ?, nombre = ?, alias = ?, rut = ?, email = ?, como = ?");
            $result = $query->execute([$candidato, $nombre, $alias, $rut, $email, implode(',', $set)]);
        } catch (PDOException $Exception) {
            error_log($Exception->getMessage( ) . ' - ' . $Exception->getCode( ));
            push_error('msg_global', 'Error al registrar el voto. Contacte al administrador.');
        }
    }
}

echo json_encode($errores);