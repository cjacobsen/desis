<?php
function push_error($id, $msg)
{
    global $errores;
    $errores[] = [
        'id' => $id,
        'msg' => $msg
    ];
}


/* 
 * Algoritmo modulo 11 (https://es.wikipedia.org/wiki/Rol_%C3%9Anico_Tributario)
 */
function rutValido($trut)
{
    $dvt = substr($trut, -1);
    $rutt = substr($trut, 0, strlen($trut) - 1);
    $rut = (int)$rutt;
    $c = 2;
    $sum = 0;
    while ($rut > 0) {
        $a1 = $rut % 10;
        $rut = floor($rut / 10);
        $sum = $sum + ($a1 * $c);
        $c = $c + 1;
        if ($c == 8) {
            $c = 2;
        }
    }
    $di = $sum % 11;
    $digi = 11 - $di;
    $digi1 = ((string) ($digi));
    if (($digi1 == '10')) {
        $digi1 = 'K';
    }
    if (($digi1 == '11')) {
        $digi1 = '0';
    }
    return ($dvt == $digi1);
}